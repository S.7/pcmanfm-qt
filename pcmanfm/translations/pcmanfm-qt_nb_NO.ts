<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nb_NO">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../about.ui" line="14"/>
        <source>About</source>
        <translation>Om</translation>
    </message>
    <message>
        <location filename="../about.ui" line="25"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:16pt; font-weight:600;&quot;&gt;PCManFM-Qt&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../about.ui" line="48"/>
        <source>Lightweight file manager</source>
        <translation>Lettvekts filhåndterer</translation>
    </message>
    <message>
        <location filename="../about.ui" line="91"/>
        <source>Programming:
* Hong Jen Yee (PCMan) &lt;pcman.tw@gmail.com&gt;
</source>
        <translation>Programmering:
* Hong Jen Yee (PCMan) &lt;pcman.tw@gmail.com&gt;
</translation>
    </message>
    <message>
        <location filename="../about.ui" line="113"/>
        <source>PCManFM-Qt File Manager

Copyright (C) 2009 - 2014 洪任諭 (Hong Jen Yee)

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.</source>
        <translation>PCManFM-Qt Filhåndterer

Opphavsrett (C) 2009 - 2014 洪任諭 (Hong Jen Yee)

Dette programmet er fri programvare. Du kan redistribuere det og/eller
forandre det under vilkårene i GNU General Public License
som publisert av Free Software Foundation, enten versjon 2
av lisensen eller (etter ditt ønske) enhver senere versjon.

Dette programmet distribueres i håpet at det vil være nyttig,
men UTEN NOEN GARANTI, til og med uten de antatte garantiene
om SALGBARHET eller VELEGNETHET FOR ET BESTEMT FORMÅL. Se
GNU General Public License for flere detaljer.

Du skal ha mottatt en kopi av GNU General Public License
sammen med dette programmet. Hvis ikke, skriv til Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.</translation>
    </message>
    <message>
        <location filename="../about.ui" line="82"/>
        <source>Authors</source>
        <translation>Opphavsmenn</translation>
    </message>
    <message>
        <location filename="../about.ui" line="58"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;https://github.com/lxqt/pcmanfm-qt&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;https://github.com/lxqt/pcmanfm-qt&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;
</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;https://github.com/lxqt/pcmanfm-qt&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;https://github.com/lxqt/pcmanfm-qt&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;
</translation>
    </message>
    <message>
        <location filename="../about.ui" line="104"/>
        <source>License</source>
        <translation>Lisens</translation>
    </message>
</context>
<context>
    <name>AutoRunDialog</name>
    <message>
        <location filename="../autorun.ui" line="14"/>
        <source>Removable medium is inserted</source>
        <translation>Uttagbart medium er satt inn</translation>
    </message>
    <message>
        <location filename="../autorun.ui" line="33"/>
        <source>&lt;b&gt;Removable medium is inserted&lt;/b&gt;</source>
        <translation>&lt;b&gt;Uttagbart medium er satt inn&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../autorun.ui" line="40"/>
        <source>Type of medium:</source>
        <translation>Type medium:</translation>
    </message>
    <message>
        <location filename="../autorun.ui" line="47"/>
        <source>Detecting...</source>
        <translation>Undersøker...</translation>
    </message>
    <message>
        <location filename="../autorun.ui" line="56"/>
        <source>Please select the action you want to perform:</source>
        <translation>Vennligst velg handlingen du ønsker å utføre:</translation>
    </message>
</context>
<context>
    <name>BulkRenameDialog</name>
    <message>
        <location filename="../bulk-rename.ui" line="6"/>
        <source>Bulk Rename</source>
        <translation>Gi nytt navn til flere</translation>
    </message>
    <message>
        <location filename="../bulk-rename.ui" line="48"/>
        <source># will be replaced by numbers starting with:</source>
        <translation># kommer til å erstattes med tall som starter med:</translation>
    </message>
    <message>
        <location filename="../bulk-rename.ui" line="71"/>
        <source>Rename selected files to:</source>
        <translation>Gi valgte filer dette navnet:</translation>
    </message>
    <message>
        <location filename="../bulk-rename.ui" line="84"/>
        <source>Name#</source>
        <translation>Navn#</translation>
    </message>
</context>
<context>
    <name>ConnectServerDialog</name>
    <message>
        <location filename="../connect.ui" line="14"/>
        <source>Connect to remote server</source>
        <translation>Koble til fjern-server</translation>
    </message>
    <message>
        <location filename="../connect.ui" line="23"/>
        <source>Anonymous &amp;login</source>
        <translation>Anonym inn&amp;logging</translation>
    </message>
    <message>
        <location filename="../connect.ui" line="36"/>
        <source>Login as &amp;user:</source>
        <translation>Log inn som br&amp;uker:</translation>
    </message>
    <message>
        <location filename="../connect.ui" line="65"/>
        <source>Specify remote folder to connect</source>
        <translation>Spesifiser mappe å koble til</translation>
    </message>
    <message>
        <location filename="../connect.ui" line="72"/>
        <source>Type:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../connect.ui" line="79"/>
        <source>Port:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../connect.ui" line="86"/>
        <source>Path:</source>
        <translation>Sti:</translation>
    </message>
    <message>
        <location filename="../connect.ui" line="96"/>
        <source>Host:</source>
        <translation>Vert:</translation>
    </message>
</context>
<context>
    <name>DesktopEntryDialog</name>
    <message>
        <location filename="../desktopentrydialog.ui" line="14"/>
        <source>Create Launcher</source>
        <translation>Skap oppstarter</translation>
    </message>
    <message>
        <location filename="../desktopentrydialog.ui" line="38"/>
        <source>Name:</source>
        <translation>Navn:</translation>
    </message>
    <message>
        <location filename="../desktopentrydialog.ui" line="55"/>
        <source>Description:</source>
        <translation>Beskrivelse:</translation>
    </message>
    <message>
        <location filename="../desktopentrydialog.ui" line="69"/>
        <source>Comment:</source>
        <translation>Kommentar:</translation>
    </message>
    <message>
        <location filename="../desktopentrydialog.ui" line="83"/>
        <source>Command:</source>
        <translation>Kommando:</translation>
    </message>
    <message>
        <location filename="../desktopentrydialog.ui" line="99"/>
        <location filename="../desktopentrydialog.ui" line="127"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../desktopentrydialog.ui" line="108"/>
        <source>Icon:</source>
        <translation>Symbol:</translation>
    </message>
    <message>
        <location filename="../desktopentrydialog.ui" line="136"/>
        <location filename="../desktopentrydialog.ui" line="146"/>
        <source>Run in terminal?</source>
        <translation>Kjør i terminal?</translation>
    </message>
    <message>
        <location filename="../desktopentrydialog.ui" line="139"/>
        <source>Terminal:</source>
        <translation>Terminal:</translation>
    </message>
    <message>
        <location filename="../desktopentrydialog.ui" line="150"/>
        <source>No</source>
        <translation>Nei</translation>
    </message>
    <message>
        <location filename="../desktopentrydialog.ui" line="155"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location filename="../desktopentrydialog.ui" line="163"/>
        <source>Type:</source>
        <translation>Type:</translation>
    </message>
    <message>
        <location filename="../desktopentrydialog.ui" line="171"/>
        <source>Application</source>
        <translation>Program</translation>
    </message>
    <message>
        <location filename="../desktopentrydialog.ui" line="176"/>
        <source>Link</source>
        <translation>Lenke</translation>
    </message>
</context>
<context>
    <name>DesktopFolder</name>
    <message>
        <location filename="../desktop-folder.ui" line="14"/>
        <source>Form</source>
        <translation>Formular</translation>
    </message>
    <message>
        <location filename="../desktop-folder.ui" line="23"/>
        <source>Desktop</source>
        <translation>Skrivebord</translation>
    </message>
    <message>
        <location filename="../desktop-folder.ui" line="29"/>
        <source>Desktop folder:</source>
        <translation>Skrivebordsmappe:</translation>
    </message>
    <message>
        <location filename="../desktop-folder.ui" line="36"/>
        <source>Image file</source>
        <translation>Bildefil</translation>
    </message>
    <message>
        <location filename="../desktop-folder.ui" line="42"/>
        <source>Folder path</source>
        <translation>Mappesti</translation>
    </message>
    <message>
        <location filename="../desktop-folder.ui" line="49"/>
        <source>&amp;Browse</source>
        <translation>&amp;Se gjennom</translation>
    </message>
</context>
<context>
    <name>DesktopPreferencesDialog</name>
    <message>
        <location filename="../desktop-preferences.ui" line="14"/>
        <source>Desktop Preferences</source>
        <translation>Skrivebordsvalg</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="247"/>
        <location filename="../desktop-preferences.ui" line="253"/>
        <source>Background</source>
        <translation>Bakgrunn</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="294"/>
        <source>Wallpaper mode:</source>
        <translation>Skrivebordsbakgrunnstype:</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="317"/>
        <source>Wallpaper image file:</source>
        <translation>Skrivebordsbakgrunnens bildefil:</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="259"/>
        <source>Select background color:</source>
        <translation>Velg bakgrunnsfarge:</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="326"/>
        <source>Image file</source>
        <translation>Bildefil</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="332"/>
        <source>Image file path</source>
        <translation>Bildefilsti</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="339"/>
        <source>&amp;Browse</source>
        <translation>&amp;Se gjennom</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="30"/>
        <source>Icons</source>
        <translation>Symboler</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="36"/>
        <source>Icon size:</source>
        <translation>Symbolstørrelse:</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="49"/>
        <source>Label Text</source>
        <translation>Etikettekst</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="113"/>
        <source>Select shadow color:</source>
        <translation>Velg skyggens farge:</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="61"/>
        <source>Select font:</source>
        <translation>Velg skrift:</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="24"/>
        <source>General</source>
        <translation>Generelt</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="87"/>
        <source>Select text color:</source>
        <translation>Velg tekstens farge:</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="136"/>
        <source>Spacing</source>
        <translation>Mellomrom</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="142"/>
        <source>Minimum item margins:</source>
        <translation>Minste marg:</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="149"/>
        <source>3 px by default.</source>
        <translation>Standard er 3 punkter.</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="152"/>
        <location filename="../desktop-preferences.ui" line="176"/>
        <source> px</source>
        <translation> pk</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="165"/>
        <source>x</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="172"/>
        <source>1 px by default.
A space is also reserved for 3 lines of text.</source>
        <translation>1 punkt er standard.
Et mellomrom er også reservert for 3 linjers tekst.</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="189"/>
        <source>Lock</source>
        <translation>Lås</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="212"/>
        <source>By default, desktop folders will be opened in PCManFM-Qt if they
are left clicked, even when it is not the default file manager.</source>
        <translation>Som forvalg vil skrivebordsmapper åpnes i PCManFM-Qt hvis de
venstreklikkes, selv når det ikke er forvalgt filbehandler.</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="216"/>
        <source>Open desktop folders in default file manager by left clicking</source>
        <translation>Åpne skrivebordsmapper i forvalgt filbehandler ved å venstreklikke</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="223"/>
        <source>Make all items stick to their positions</source>
        <translation>Fest alle elementer til sine posisjoner</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="282"/>
        <source>Wallpaper</source>
        <translation>Skrivebordsbakgrunn</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="348"/>
        <source>Transform image based on EXIF data</source>
        <translation>Forandre bilde basert på EXIF-data</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="355"/>
        <source>Individual wallpaper for each monitor</source>
        <translation>Eget bakgrunnsbilde for hver skjerm</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="382"/>
        <source>Slide Show</source>
        <translation>Lysbildeshow</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="388"/>
        <source>Enable Slide Show</source>
        <translation>Aktiver lysbildeshow</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="400"/>
        <source>Wallpaper image folder:</source>
        <translation>Mappe for skrivebordsbakgrunner:</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="407"/>
        <source>Browse</source>
        <translation>Se gjennom</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="414"/>
        <source> hour(s)</source>
        <translation> time(r)</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="424"/>
        <source>and</source>
        <translation>og</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="437"/>
        <source>Intervals less than 5min will be ignored</source>
        <translation>Intervaller mindre enn 5 minutter ingoreres</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="440"/>
        <source>Interval:</source>
        <translation>Intervall:</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="447"/>
        <source> minute(s)</source>
        <translation> minutte(r)</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="473"/>
        <source>Wallpaper folder</source>
        <translation>Skrivebordsbakgrunnsmappe</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="496"/>
        <source>Randomize the slide show</source>
        <translation>Tilfeldig bilde i lysbildeshow</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="529"/>
        <source>Visible Shortcuts</source>
        <translation>Synlige snareveier</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="535"/>
        <source>Home</source>
        <translation>Hjem</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="546"/>
        <source>Trash</source>
        <translation>Papirkurv</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="557"/>
        <source>Computer</source>
        <translation>Datamaskin</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="568"/>
        <source>Network</source>
        <translation>Nettverk</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="523"/>
        <source>Advanced</source>
        <translation>Avansert</translation>
    </message>
</context>
<context>
    <name>HiddenShortcutsDialog</name>
    <message>
        <location filename="../shortcuts.ui" line="14"/>
        <source>Hidden Shortcuts</source>
        <translation>Skjulte snarveier</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="21"/>
        <source>Shortcut</source>
        <translation>Snarvei</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="26"/>
        <source>Action</source>
        <translation>Handling</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="31"/>
        <source>Esc</source>
        <translation>Esc</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="34"/>
        <source>Focus view, clear filter bar</source>
        <translation>Fokuser visning, tøm filterlinje</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="39"/>
        <source>Ctrl+Esc</source>
        <translation>Ctrl+Esc</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="42"/>
        <source>Focus side-pane</source>
        <translation>Fokuser sidefelt</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="47"/>
        <source>Ctrl+L</source>
        <translation>Ctrl+L</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="50"/>
        <location filename="../shortcuts.ui" line="58"/>
        <source>Focus path entry</source>
        <translation>Fokuser sti-inntasting</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="55"/>
        <source>Alt+D</source>
        <translation>Alt+D</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="63"/>
        <source>Ctrl+Tab</source>
        <translation>Ctrl+Tab</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="66"/>
        <location filename="../shortcuts.ui" line="82"/>
        <source>Next tab</source>
        <translation>Neste fane</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="71"/>
        <source>Ctrl+Shift+Tab</source>
        <translation>Ctrl+Shift+Tab</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="74"/>
        <location filename="../shortcuts.ui" line="90"/>
        <source>Previous tab</source>
        <translation>Forrige fane</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="79"/>
        <source>Ctrl+PageDown</source>
        <translation>Ctrl+PageDown</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="87"/>
        <source>Ctrl+PageUp</source>
        <translation>Ctrl+PageUp</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="95"/>
        <source>Ctrl+Number</source>
        <translation>Ctrl+Nummer</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="98"/>
        <location filename="../shortcuts.ui" line="106"/>
        <source>Jump to tab</source>
        <translation>Hopp til fane</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="103"/>
        <source>Alt+Number</source>
        <translation>Alt+Nummer</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="111"/>
        <source>Backspace</source>
        <translation>Rettetast</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="114"/>
        <source>Go up</source>
        <translation>Gå oppover</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="119"/>
        <source>Ctrl+F</source>
        <translation>Ctrl+F</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="122"/>
        <source>Search dialog</source>
        <translation>Søkedialog</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="127"/>
        <source>Shift+Insert</source>
        <translation>Shift+Insert</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="130"/>
        <source>Paste into transient filter bar</source>
        <translation>Lim inn i forbigående filterlinje</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="135"/>
        <source>Drag+Shift</source>
        <translation>Dra+Shift</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="138"/>
        <source>Move file on dropping</source>
        <translation>Flytt fil når den slippes</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="143"/>
        <source>Drag+Ctrl</source>
        <translation>Dra+Ctrl</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="146"/>
        <source>Copy file on dropping</source>
        <translation>Kopier fil når den slippes</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="151"/>
        <source>Drag+Shift+Ctrl</source>
        <translation>Dra+Shift+Ctrl</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="154"/>
        <source>Make a symlink on dropping</source>
        <translation>Lag en symbolsk lenke når den slippes</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../main-win.ui" line="14"/>
        <source>File Manager</source>
        <translation>Filhåndterer</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="133"/>
        <source>&amp;Toolbars</source>
        <translation>&amp;Verktøylinjer</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="140"/>
        <source>Path &amp;Bar</source>
        <translation>Sti&amp;linje</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="147"/>
        <source>&amp;Filtering</source>
        <translation>&amp;Filtrering</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="221"/>
        <source>&amp;Tools</source>
        <translation>&amp;Verktøy</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="280"/>
        <source>Go &amp;Up</source>
        <translation>Gå &amp;opp</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="283"/>
        <source>Go Up</source>
        <translation>Gå opp</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="286"/>
        <source>Alt+Up</source>
        <translation>Alt+opp</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="295"/>
        <source>&amp;Home</source>
        <translation>&amp;Hjem</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="298"/>
        <source>Alt+Home</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="307"/>
        <source>&amp;Reload</source>
        <translation>&amp;Last inn på nytt</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="310"/>
        <source>F5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="319"/>
        <source>Go</source>
        <translation>Gå</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="328"/>
        <source>Quit</source>
        <translation>Avslutt</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="337"/>
        <source>&amp;About</source>
        <translation>&amp;Om</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="346"/>
        <source>&amp;New Window</source>
        <translation>&amp;Nytt vindu</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="349"/>
        <source>New Window</source>
        <translation>Nytt vindu</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="352"/>
        <source>Ctrl+N</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="360"/>
        <source>Show &amp;Hidden</source>
        <translation>Vis &amp;skjulte</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="363"/>
        <source>Ctrl+H</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="372"/>
        <source>&amp;Computer</source>
        <translation>&amp;Datamaskin</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="381"/>
        <source>&amp;Trash</source>
        <translation>&amp;Papirkurv</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="386"/>
        <source>&amp;Network</source>
        <translation>&amp;Nettverk</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="395"/>
        <source>&amp;Desktop</source>
        <translation>&amp;Skrivebord</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="404"/>
        <source>&amp;Add to Bookmarks</source>
        <translation>&amp;Legg til bokmerker</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="409"/>
        <source>&amp;Applications</source>
        <translation>&amp;Programmer</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="414"/>
        <source>Reload</source>
        <translation>Last inn på nytt</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="422"/>
        <source>&amp;Icon View</source>
        <translation>&amp;Symbolvisning</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="430"/>
        <source>&amp;Compact View</source>
        <translation>&amp;Kompakt visning</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="438"/>
        <source>&amp;Detailed List</source>
        <translation>&amp;Detaljert liste</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="446"/>
        <source>&amp;Thumbnail View</source>
        <translation>&amp;Minibildevisning</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="455"/>
        <source>Cu&amp;t</source>
        <translation>Kli&amp;pp ut</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="458"/>
        <source>Ctrl+X</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="467"/>
        <source>&amp;Copy</source>
        <translation>&amp;Kopier</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="470"/>
        <source>Ctrl+C</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="479"/>
        <source>&amp;Paste</source>
        <translation>&amp;Lim inn</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="482"/>
        <source>Ctrl+V</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="487"/>
        <source>Select &amp;All</source>
        <translation>Velg &amp;alt</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="490"/>
        <source>Ctrl+A</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="495"/>
        <source>Pr&amp;eferences</source>
        <translation>V&amp;alg</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="503"/>
        <source>&amp;Ascending</source>
        <translation>&amp;Stigende</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="511"/>
        <source>&amp;Descending</source>
        <translation>S&amp;ynkende</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="519"/>
        <source>&amp;By File Name</source>
        <translation>Etter fi&amp;lnavn</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="527"/>
        <source>By &amp;Modification Time</source>
        <translation>Etter &amp;sist endret</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="535"/>
        <source>By C&amp;reation Time</source>
        <translation>Ette&amp;r dato</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="543"/>
        <source>By Deletio&amp;n Time</source>
        <translation>Etter &amp;tid slettet</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="551"/>
        <source>By File &amp;Type</source>
        <translation>Etter fil&amp;type</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="559"/>
        <source>By &amp;Owner</source>
        <translation>Etter &amp;eier</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="567"/>
        <source>By &amp;Group</source>
        <translation>Etter &amp;gruppe</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="575"/>
        <source>&amp;Folder First</source>
        <translation>&amp;Mapper først</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="583"/>
        <source>&amp;Hidden Last</source>
        <translation>&amp;Skjult sist</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="948"/>
        <source>Preserve Settings Recursively from &amp;Here</source>
        <translation type="unfinished">Behold innstillinger rekursivt &amp;herfra</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="953"/>
        <source>&amp;Go to Source of Inherited Settings</source>
        <translation>&amp;Gå til kilden for nedarvede innstillinger</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="958"/>
        <source>&amp;Remove Settings of Nonexistent Folders</source>
        <translation>&amp;Fjern innstillinger for ikke-eksisterende mapper</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="636"/>
        <source>Ctrl+Shift+A</source>
        <translation>Ctrl+Shift+A</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="685"/>
        <source>&amp;Case Sensitive</source>
        <translation>&amp;Forskjell på STORE og små bokstaver</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="693"/>
        <source>By File &amp;Size</source>
        <translation>Etter &amp;størrelse</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="698"/>
        <source>&amp;Close Window</source>
        <translation>&amp;Lukk vindu</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="701"/>
        <source>Ctrl+Q</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="719"/>
        <source>Open Tab in &amp;Root Instance</source>
        <translation>Åpne fane i &amp;rot-instans</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="733"/>
        <source>&amp;Folder</source>
        <translation>&amp;Mappe</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="745"/>
        <source>&amp;Blank File</source>
        <translation>&amp;Tom fil</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="832"/>
        <source>Preserve Settings for &amp;This Folder</source>
        <translation>Behold innstillinger for &amp;denne mappen</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="869"/>
        <source>&amp;Show/Focus Filter Bar</source>
        <translation>&amp;Vis/gi fokus til filterlinje</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="872"/>
        <source>Show Filter Bar</source>
        <translation>Vis filterlinje</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="875"/>
        <source>Ctrl+I</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="883"/>
        <source>S&amp;plit View</source>
        <translation>&amp;Delt visning</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="886"/>
        <source>Split View</source>
        <translation>Delt visning</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="889"/>
        <source>F6</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="894"/>
        <source>&amp;Copy Full Path</source>
        <translation>&amp;Kopier hele stien</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="897"/>
        <source>Ctrl+Shift+C</source>
        <translation>Ctrl+Shift+C</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="908"/>
        <source>Show Thumb&amp;nails</source>
        <translation>Vis forhå&amp;ndsvisninger</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="911"/>
        <source>Show Thumbnails</source>
        <translation>Vis forhåndsvisninger</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="919"/>
        <source>S&amp;ide Pane</source>
        <translation>&amp;Siderute</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="922"/>
        <source>F9</source>
        <translation>F9</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="927"/>
        <source>Hidden &amp;Shortcuts</source>
        <translation>Skjulte &amp;snarveier</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="932"/>
        <source>Open Tab in &amp;Admin Mode</source>
        <translation>Åpne fane i &amp;administratormodus</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="937"/>
        <location filename="../main-win.ui" line="940"/>
        <source>Create Launcher</source>
        <translation type="unfinished">Skap oppstarter</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="767"/>
        <source>Ctrl+B</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="772"/>
        <source>&amp;Clear All Filters</source>
        <translation>&amp;Fjern alle filtre</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="775"/>
        <source>Ctrl+Shift+K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="784"/>
        <source>Close &amp;previous tabs</source>
        <translation>Lukk &amp;forrige fane</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="793"/>
        <source>Close &amp;next tabs</source>
        <translation>Lukk &amp;neste fane</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="837"/>
        <source>Connect to &amp;Server</source>
        <translation>Koble til &amp;server</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="845"/>
        <source>&amp;Location</source>
        <translation>&amp;Plassering</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="853"/>
        <source>&amp;Path Buttons</source>
        <translation>&amp;Stiknapper</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="858"/>
        <source>&amp;Bulk Rename</source>
        <translation>&amp;Gi nytt navn til flere</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="861"/>
        <source>Bulk Rename</source>
        <translation>Gi nytt navn til flere</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="864"/>
        <source>Ctrl+F2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="798"/>
        <source>Close &amp;other tabs</source>
        <translation>Lukk &amp;andre faner</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="764"/>
        <source>Permanent &amp;filter bar</source>
        <translation>Permanent &amp;filterlinje</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="806"/>
        <source>&amp;Menu bar</source>
        <translation>&amp;Menylinje</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="809"/>
        <source>Menu bar</source>
        <translation>Menylinje</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="812"/>
        <source>Ctrl+M</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="821"/>
        <location filename="../main-win.ui" line="824"/>
        <source>Menu</source>
        <translation>Meny</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="592"/>
        <source>New &amp;Tab</source>
        <translation>Ny &amp;fane</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="154"/>
        <source>&amp;Customized View Settings</source>
        <translation>&amp;Tilpassede visningsinnstillinger</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="595"/>
        <source>New Tab</source>
        <translation>Ny fane</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="598"/>
        <source>Ctrl+T</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="607"/>
        <source>Go &amp;Back</source>
        <translation>Gå til&amp;bake</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="610"/>
        <source>Go Back</source>
        <translation>Gå tilbake</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="613"/>
        <source>Alt+Left</source>
        <translation>Alt+venstre</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="622"/>
        <source>Go &amp;Forward</source>
        <translation>Gå &amp;framover</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="625"/>
        <source>Go Forward</source>
        <translation>Gå framover</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="628"/>
        <source>Alt+Right</source>
        <translation>Alt+høyre</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="633"/>
        <source>&amp;Invert Selection</source>
        <translation>&amp;Inverter markering</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="645"/>
        <source>&amp;Delete</source>
        <translation>&amp;Slett</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="648"/>
        <source>Del</source>
        <translation>Del</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="653"/>
        <source>&amp;Rename</source>
        <translation>&amp;Gi nytt navn</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="656"/>
        <source>F2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="661"/>
        <source>C&amp;lose Tab</source>
        <translation>&amp;Lukk fane</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="664"/>
        <source>Ctrl+W</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="669"/>
        <source>File &amp;Properties</source>
        <translation>Fil&amp;egenskaper</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="672"/>
        <source>Alt+Return</source>
        <translation>Alt+Retur</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="677"/>
        <source>&amp;Folder Properties</source>
        <translation>&amp;Mappeegenskaper</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="706"/>
        <source>Edit Bookmarks</source>
        <translation>Rediger bokmerker</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="711"/>
        <source>Open &amp;Terminal</source>
        <translation>Åpne &amp;terminal</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="714"/>
        <source>F4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="724"/>
        <source>&amp;Edit Bookmarks</source>
        <translation>R&amp;ediger bokmerker</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="736"/>
        <source>Ctrl+Shift+N</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="748"/>
        <source>Ctrl+Alt+N</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="753"/>
        <source>&amp;Find Files</source>
        <translation>&amp;Finn filer</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="756"/>
        <source>F3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="70"/>
        <source>&amp;File</source>
        <translation>&amp;Fil</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="74"/>
        <source>C&amp;reate New</source>
        <translation>L&amp;ag ny</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="92"/>
        <source>&amp;Help</source>
        <translation>&amp;Hjelp</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="99"/>
        <location filename="../main-win.ui" line="123"/>
        <source>&amp;View</source>
        <translation>&amp;Visning</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="103"/>
        <source>&amp;Sorting</source>
        <translation>&amp;Sortering</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="179"/>
        <source>&amp;Edit</source>
        <translation>R&amp;ediger</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="197"/>
        <source>&amp;Bookmarks</source>
        <translation>&amp;Bokmerker</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="204"/>
        <source>&amp;Go</source>
        <translation>&amp;Gå</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="246"/>
        <source>Main Toolbar</source>
        <translation>Hovedverktøylinje</translation>
    </message>
</context>
<context>
    <name>PCManFM::Application</name>
    <message>
        <location filename="../application.cpp" line="168"/>
        <source>Name of configuration profile</source>
        <translation>Navn på konfigurasjonsprofil</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="168"/>
        <source>PROFILE</source>
        <translation>Profil</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="171"/>
        <source>Run PCManFM-Qt as a daemon</source>
        <translation>Kjør PCManFM-Qt som nisse</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="174"/>
        <source>Quit PCManFM-Qt</source>
        <translation>Avslut PCManFM-Qt</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="177"/>
        <source>Launch desktop manager</source>
        <translation>Kjør skrivebordshåndterer</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="180"/>
        <source>Turn off desktop manager if it&apos;s running</source>
        <translation>Slå av skrivebordshåndterer hvis den kjører</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="183"/>
        <source>Open desktop preference dialog on the page with the specified name</source>
        <translation>Åpne skrivebordsvalg på siden med det spesifiserte navnet</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="183"/>
        <location filename="../application.cpp" line="198"/>
        <source>NAME</source>
        <translation>Navn</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="186"/>
        <source>Open new window</source>
        <translation>Åpne nytt vindu</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="189"/>
        <source>Open Find Files utility</source>
        <translation>Åpne finn filer-verktøyet</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="192"/>
        <source>Set desktop wallpaper from image FILE</source>
        <translation>Sett skrivebordsbakgrunn fra bildefil</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="192"/>
        <source>FILE</source>
        <translation>Fil</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="195"/>
        <source>MODE</source>
        <translation>Modus</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="195"/>
        <source>Set mode of desktop wallpaper. MODE=(%1)</source>
        <translation>Sett modus for skrivebordsbakgrunn. Modus=(%1)</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="198"/>
        <source>Open Preferences dialog on the page with the specified name</source>
        <translation>Åpne valg på siden med det spesifiserte navnet</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="201"/>
        <source>Files or directories to open</source>
        <translation>Filer og mapper å åpne</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="201"/>
        <source>[FILE1, FILE2,...]</source>
        <translation>[FIL1, FIL2,...]</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="627"/>
        <location filename="../application.cpp" line="632"/>
        <source>Error</source>
        <translation>Feil</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="632"/>
        <source>Terminal emulator is not set.</source>
        <translation>Terminalemulator er ikke stilt inn.</translation>
    </message>
</context>
<context>
    <name>PCManFM::AutoRunDialog</name>
    <message>
        <location filename="../autorundialog.cpp" line="44"/>
        <source>Open in file manager</source>
        <translation>Åpne i filhåndtereren</translation>
    </message>
    <message>
        <location filename="../autorundialog.cpp" line="138"/>
        <source>Removable Disk</source>
        <translation>Uttagbar disk</translation>
    </message>
</context>
<context>
    <name>PCManFM::ConnectServerDialog</name>
    <message>
        <location filename="../connectserverdialog.cpp" line="9"/>
        <source>SSH</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../connectserverdialog.cpp" line="10"/>
        <source>FTP</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../connectserverdialog.cpp" line="11"/>
        <source>WebDav</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../connectserverdialog.cpp" line="12"/>
        <source>Secure WebDav</source>
        <translation>Sikker WebDav</translation>
    </message>
    <message>
        <location filename="../connectserverdialog.cpp" line="13"/>
        <source>HTTP</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../connectserverdialog.cpp" line="14"/>
        <source>HTTPS</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>PCManFM::DesktopEntryDialog</name>
    <message>
        <location filename="../desktopentrydialog.cpp" line="29"/>
        <source>Command:</source>
        <translation>Kommando:</translation>
    </message>
    <message>
        <location filename="../desktopentrydialog.cpp" line="32"/>
        <source>URL:</source>
        <translation>Nettadresse:</translation>
    </message>
    <message>
        <location filename="../desktopentrydialog.cpp" line="57"/>
        <source>Select an icon</source>
        <translation>Velg et symbol</translation>
    </message>
    <message>
        <location filename="../desktopentrydialog.cpp" line="59"/>
        <source>Images (*.png *.xpm *.svg *.svgz )</source>
        <translation>Bilder (*.png *.xpm *.svg *.svgz )</translation>
    </message>
    <message>
        <location filename="../desktopentrydialog.cpp" line="81"/>
        <source>Select an executable file</source>
        <translation>Velg en kjørbar fil</translation>
    </message>
    <message>
        <location filename="../desktopentrydialog.cpp" line="89"/>
        <source>Select a file</source>
        <translation>Velg en fil</translation>
    </message>
</context>
<context>
    <name>PCManFM::DesktopPreferencesDialog</name>
    <message>
        <location filename="../desktoppreferencesdialog.cpp" line="53"/>
        <source>Fill with background color only</source>
        <translation>Fyll bare med bakgrunnsfarge</translation>
    </message>
    <message>
        <location filename="../desktoppreferencesdialog.cpp" line="54"/>
        <source>Stretch to fill the entire screen</source>
        <translation>Strekk for å fylle hele skjermen</translation>
    </message>
    <message>
        <location filename="../desktoppreferencesdialog.cpp" line="55"/>
        <source>Stretch to fit the screen</source>
        <translation>Strekk for å passe til skjermen</translation>
    </message>
    <message>
        <location filename="../desktoppreferencesdialog.cpp" line="56"/>
        <source>Center on the screen</source>
        <translation>Sentrer på skjermen</translation>
    </message>
    <message>
        <location filename="../desktoppreferencesdialog.cpp" line="57"/>
        <source>Tile the image to fill the entire screen</source>
        <translation>Gjenta bildet for å fylle hele skjermen</translation>
    </message>
    <message>
        <location filename="../desktoppreferencesdialog.cpp" line="58"/>
        <source>Zoom the image to fill the entire screen</source>
        <translation>Zoom bildet for å fylle hele skjermen</translation>
    </message>
    <message>
        <location filename="../desktoppreferencesdialog.cpp" line="255"/>
        <source>Select Wallpaper</source>
        <translation>Velg skrivebordsbakgrunn</translation>
    </message>
    <message>
        <location filename="../desktoppreferencesdialog.cpp" line="259"/>
        <source>Image Files</source>
        <translation>Bildefiler</translation>
    </message>
    <message>
        <location filename="../desktoppreferencesdialog.cpp" line="301"/>
        <source>Select Wallpaper Folder</source>
        <translation>Velg bakgrunnsbildemappe</translation>
    </message>
</context>
<context>
    <name>PCManFM::DesktopWindow</name>
    <message>
        <location filename="../desktopwindow.cpp" line="299"/>
        <source>Trash (One item)</source>
        <translation>Papirkurv (ett element)</translation>
    </message>
    <message numerus="yes">
        <location filename="../desktopwindow.cpp" line="302"/>
        <source>Trash (%Ln items)</source>
        <translation>
            <numerusform>Papirkurv (%Ln elementer)</numerusform>
            <numerusform>Papirkurv (%Ln objekter)</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../desktopwindow.cpp" line="306"/>
        <source>Trash (Empty)</source>
        <translation>Papirkurv (tom)</translation>
    </message>
    <message>
        <location filename="../desktopwindow.cpp" line="341"/>
        <source>Computer</source>
        <translation>Datamaskin</translation>
    </message>
    <message>
        <location filename="../desktopwindow.cpp" line="355"/>
        <source>Network</source>
        <translation>Nettverk</translation>
    </message>
    <message>
        <location filename="../desktopwindow.cpp" line="963"/>
        <source>Open</source>
        <translation>Åpne</translation>
    </message>
    <message>
        <location filename="../desktopwindow.cpp" line="969"/>
        <location filename="../desktopwindow.cpp" line="1009"/>
        <source>Stic&amp;k to Current Position</source>
        <translation>&amp;Sett fast i nåværende posisjon</translation>
    </message>
    <message>
        <location filename="../desktopwindow.cpp" line="977"/>
        <source>Empty Trash</source>
        <translation>Tøm papirkurv</translation>
    </message>
    <message>
        <location filename="../desktopwindow.cpp" line="1036"/>
        <source>Hide Desktop Items</source>
        <translation>Skjul skrivebordselementer</translation>
    </message>
    <message>
        <location filename="../desktopwindow.cpp" line="1042"/>
        <source>Create Launcher</source>
        <translation>Skap oppstarter</translation>
    </message>
    <message>
        <location filename="../desktopwindow.cpp" line="1045"/>
        <source>Desktop Preferences</source>
        <translation>Skrivebordsvalg</translation>
    </message>
</context>
<context>
    <name>PCManFM::FilterBar</name>
    <message>
        <location filename="../tabpage.cpp" line="93"/>
        <source>Filter:</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>PCManFM::FilterEdit</name>
    <message>
        <location filename="../tabpage.cpp" line="63"/>
        <source>Clear text (Ctrl+K or Esc)</source>
        <translation>Slett tekst (Ctrl+K eller Esc)</translation>
    </message>
</context>
<context>
    <name>PCManFM::MainWindow</name>
    <message>
        <location filename="../mainwindow.cpp" line="141"/>
        <source>Root Instance</source>
        <translation>Rot-instans</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="724"/>
        <source>Hide menu bar</source>
        <translation>Skjul menylinje</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="725"/>
        <source>This will hide the menu bar completely, use Ctrl+M to show it again.</source>
        <translation>Dette vil skjule menylinjen helt. Bruk Ctrl+M for å vise den igjen.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1097"/>
        <source>Version: %1</source>
        <translation>Versjon: %1</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="174"/>
        <location filename="../mainwindow.cpp" line="2057"/>
        <source>&amp;Move to Trash</source>
        <translation>Flytt til &amp;papirkurven</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="174"/>
        <location filename="../mainwindow.cpp" line="2057"/>
        <source>&amp;Delete</source>
        <translation>&amp;Slett</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1900"/>
        <source>Customized View Settings</source>
        <translation type="unfinished">Innstillinger for tilpasset visning</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2125"/>
        <location filename="../mainwindow.cpp" line="2133"/>
        <source>Error</source>
        <translation>Feil</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2133"/>
        <source>Switch user command is not set.</source>
        <translation>Skiftbrukerkommando er ikke innstilt.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2235"/>
        <source>Cleaning Folder Settings</source>
        <translation type="unfinished">Innstillinger for tømming av mapper</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2236"/>
        <source>Do you want to remove settings of nonexistent folders?
They might be useful if those folders are created again.</source>
        <translation>Fjern innstillinger for ikke-eksisterende mapper?
De kan være nyttige hvis disse mappene opprettes igjen.</translation>
    </message>
</context>
<context>
    <name>PCManFM::PreferencesDialog</name>
    <message>
        <location filename="../preferencesdialog.cpp" line="199"/>
        <source>Icon View</source>
        <translation>Symbolvisning</translation>
    </message>
    <message>
        <location filename="../preferencesdialog.cpp" line="200"/>
        <source>Compact View</source>
        <translation>Kompakt visning</translation>
    </message>
    <message>
        <location filename="../preferencesdialog.cpp" line="201"/>
        <source>Thumbnail View</source>
        <translation>Minibildevisning</translation>
    </message>
    <message>
        <location filename="../preferencesdialog.cpp" line="202"/>
        <source>Detailed List View</source>
        <translation>Detaljert listevisning</translation>
    </message>
</context>
<context>
    <name>PCManFM::TabPage</name>
    <message>
        <location filename="../tabpage.cpp" line="506"/>
        <source>Error</source>
        <translation>Feil</translation>
    </message>
    <message>
        <location filename="../tabpage.cpp" line="515"/>
        <source>Free space: %1 (Total: %2)</source>
        <translation>Ledig plass: %1 (Totalt: %2)</translation>
    </message>
    <message numerus="yes">
        <location filename="../tabpage.cpp" line="532"/>
        <source>%n item(s)</source>
        <translation>
            <numerusform>%n element</numerusform>
            <numerusform>%n elementer</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../tabpage.cpp" line="534"/>
        <source> (%n hidden)</source>
        <translation>
            <numerusform> (%n skjult)</numerusform>
            <numerusform> (%n skjulte)</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../tabpage.cpp" line="539"/>
        <location filename="../tabpage.cpp" line="735"/>
        <location filename="../tabpage.cpp" line="749"/>
        <source>Link to</source>
        <translation>Lenke til</translation>
    </message>
    <message numerus="yes">
        <location filename="../tabpage.cpp" line="761"/>
        <source>%n item(s) selected</source>
        <translation>
            <numerusform>%n valgt element</numerusform>
            <numerusform>%n valgte elementer</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>PCManFM::View</name>
    <message>
        <location filename="../view.cpp" line="59"/>
        <source>Many files</source>
        <translation>Mange filer</translation>
    </message>
    <message numerus="yes">
        <location filename="../view.cpp" line="60"/>
        <source>Do you want to open these %1 files?</source>
        <translation>
            <numerusform>Ønsker å åpne denne %1 filen?</numerusform>
            <numerusform>Ønsker du å åpne disse %1 filene?</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../view.cpp" line="122"/>
        <source>Open in New T&amp;ab</source>
        <translation>Åpne i ny f&amp;ane</translation>
    </message>
    <message>
        <location filename="../view.cpp" line="126"/>
        <source>Open in New Win&amp;dow</source>
        <translation>Åpne i nytt &amp;vindu</translation>
    </message>
    <message>
        <location filename="../view.cpp" line="134"/>
        <source>Open in Termina&amp;l</source>
        <translation>Åpne i termina&amp;l</translation>
    </message>
</context>
<context>
    <name>PreferencesDialog</name>
    <message>
        <location filename="../preferences.ui" line="14"/>
        <source>Preferences</source>
        <translation>Innstillinger</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="45"/>
        <source>User Interface</source>
        <translation>Brukergrensesnitt</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="35"/>
        <source>Behavior</source>
        <translation>Oppførsel</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="50"/>
        <location filename="../preferences.ui" line="605"/>
        <source>Thumbnail</source>
        <translation>Minibilde</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="55"/>
        <source>Volume</source>
        <translation>Volum</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="60"/>
        <source>Advanced</source>
        <translation>Avansert</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="222"/>
        <source>Select newly created files</source>
        <translation>Velg nylig opprettede filer</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="259"/>
        <source>Icons</source>
        <translation>Symboler</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="291"/>
        <source>Size of big icons:</source>
        <translation>Størrelse på store symboler:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="308"/>
        <source>Size of small icons:</source>
        <translation>Størrelse på små symboler:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="325"/>
        <source>Size of thumbnails:</source>
        <translation>Størrelse på minibilder:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="339"/>
        <source>Size of side pane icons:</source>
        <translation>Størrelse på symboler i sidepanelet:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="271"/>
        <source>Icon theme:</source>
        <translation>Symboltema:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="509"/>
        <source>Window</source>
        <translation>Vindu</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="522"/>
        <source>Always show the tab bar</source>
        <translation>Alltid vis fanelinjen</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="529"/>
        <source>Show &apos;Close&apos; buttons on tabs	</source>
        <translation>Vis lukkeknapper i faner	</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="543"/>
        <source>Remember the size of the last closed window</source>
        <translation>Husk størrelsen på det sist lukkede vinduet</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="550"/>
        <source>Default width of new windows:</source>
        <translation>Standardbredde for nye vinduer:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="564"/>
        <source>Default height of new windows:</source>
        <translation>Standardhøyde for nye vinduer:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="81"/>
        <source>Browsing</source>
        <translation>Gjennomsyn</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="93"/>
        <source>Open files with single click</source>
        <translation>Åpne filer med ett klikk</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="113"/>
        <source>Default view mode:</source>
        <translation>Standardvisning:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="129"/>
        <source> sec</source>
        <translation> sek.</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="175"/>
        <source>File Operations</source>
        <translation>Filhandlinger</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="181"/>
        <source>Confirm before deleting files</source>
        <translation>Bekreft før sletting av filer</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="188"/>
        <source>Move deleted files to &quot;trash bin&quot; instead of erasing from disk.</source>
        <translation>Flytt slettede filer til papirkurven i stedet for å for å slette dem fra disken.</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="614"/>
        <source>Show thumbnails of files</source>
        <translation>Vis minibilder av filer</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="621"/>
        <source>Only show thumbnails for local files</source>
        <translation>Bare vis minibilder for lokale filer</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="40"/>
        <source>Display</source>
        <translation>Visning</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="90"/>
        <source>If this is unchecked, the DE setting will be used.</source>
        <translation>Hvis umarkert vil innstillingene fra skrivebordsmiljøet brukes.</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="103"/>
        <location filename="../preferences.ui" line="126"/>
        <source>Set to zero to disable auto-selection.</source>
        <translation>Sett til null for å skru av automatisk utvalg.</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="106"/>
        <source>Delay of auto-selection in single click mode:</source>
        <translation>Forsinkelse for auto-markering i enkeltklikksmodus:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="147"/>
        <source>Open in current tab</source>
        <translation>Åpne i nåværende fane</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="152"/>
        <source>Open in new tab</source>
        <translation>Åpne i ny fane</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="157"/>
        <source>Open in new window</source>
        <translation>Åpne i nytt vindu</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="195"/>
        <source>Erase files on removable media instead of &quot;trash can&quot; creation</source>
        <translation>Slett filer på flyttbare medier i stedet for å legge dem i papirkurven</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="202"/>
        <source>Confirm before moving files into &quot;trash can&quot;</source>
        <translation>Bekreft flytting av filer til papirkurven</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="209"/>
        <location filename="../preferences.ui" line="372"/>
        <location filename="../preferences.ui" line="382"/>
        <source>Requires application restart to take effect completely</source>
        <translation>Krever omstart av programmet for å tre helt i kraft</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="212"/>
        <source>Launch executable files without prompt</source>
        <translation>Kjør kjørbare filer uten å spør</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="219"/>
        <source>Renamed files will also be selected</source>
        <translation>Filer med endret navn vil også bli valgt</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="288"/>
        <location filename="../preferences.ui" line="298"/>
        <source>Used by Icon View</source>
        <translation>Brukt av symbolvisning</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="305"/>
        <location filename="../preferences.ui" line="315"/>
        <source>Used by Compact View and Detailed List View</source>
        <translation>Brukt av kompaktvisning og detaljert listevisning</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="322"/>
        <location filename="../preferences.ui" line="332"/>
        <source>Used by Thumbnail View</source>
        <translation>Brukt av minibildevisning</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="352"/>
        <source>User interface</source>
        <translation>Brukergrensesnitt</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="365"/>
        <source>Treat backup files as hidden</source>
        <translation>Behandle sikkerhetskopifiler som skjulte</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="385"/>
        <source>Show icons of hidden files shadowed</source>
        <translation>Vis skyggelagte symboler for skjulte filer</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="399"/>
        <source>Disable smooth scrolling in list and compact modes</source>
        <translation>Skru av myk rulling i kompakt- og listemodus</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="422"/>
        <source>Minimum item margins in icon view:</source>
        <translation>Minimum marg i symbolvisning:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="429"/>
        <source>3 px by default.</source>
        <translation>Standard er 3 punkter.</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="432"/>
        <location filename="../preferences.ui" line="456"/>
        <source> px</source>
        <translation> pk</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="445"/>
        <source>x</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="452"/>
        <source>3 px by default.
A space is also reserved for 3 lines of text.</source>
        <translation>3 punkter er standard.
Et mellomrom er også reservert for tre linjers tekst.</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="469"/>
        <source>Lock</source>
        <translation>Lås</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="518"/>
        <source>When unchecked, the tab bar will be shown
only if there are more than one tab.</source>
        <translation>Når ikke avkrysset vil fanelinjen bare vises
hvis det er mer enn en fane.</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="536"/>
        <source>Switch to newly opened tab</source>
        <translation>Gå til nylig åpnet fane</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="578"/>
        <source>Reopen last window tabs in a new window</source>
        <translation>Gjenåpne siste vindus faner i et nytt vindu</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="628"/>
        <location filename="../preferences.ui" line="640"/>
        <source>The built-in thumbnailer makes thumbnails of images that are supported by Qt.

Usually, most image types are supported. The default size limit is 4 MiB.</source>
        <translation>Programmet som lager småikoner gjør det bare av bilder som er støttet av Qt.

Vanligvis er de fleste bildetyper støttet. Begrensningen i størrelse er 4 MiB som standard.</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="633"/>
        <source>Image size limit for built-in thumbnailer:</source>
        <translation>Maks størrelse for innebygde små-ikoner:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="645"/>
        <location filename="../preferences.ui" line="681"/>
        <source> MiB</source>
        <translation> MiB</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="661"/>
        <location filename="../preferences.ui" line="673"/>
        <source>If existing, external thumbnailers are used for videos, PDF documents, etc.

A value of -1 means that there is no limit for the file size (the default).</source>
        <translation>Hvis de fins brukes eksterne småikonsprogrammer for videoer, PDF-dokumenter osv.

En verdi på -1 betyr at det ikke er noen begrensning på filstørrelsen (standard).</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="666"/>
        <source>File size limit for external thumbnailers:</source>
        <translation>Maksimal filstørrelse for eksterne småikonsprogrammer:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="678"/>
        <source>No limit</source>
        <translation>Ingen begrensning</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="816"/>
        <source>Examples:&lt;br&gt;For terminal: &lt;i&gt;xterm -e %s&lt;/i&gt;&lt;br&gt;For switching user: &lt;i&gt;lxsudo %s&lt;/i&gt; or &lt;i&gt;lxsudo dbus-run-session -- %s&lt;/i&gt;&lt;br&gt;&lt;i&gt;%s&lt;/i&gt; is the command line you want to execute with terminal or su.&lt;br&gt; Important: Please use lxsudo, sudo alone will wreck permissions of the settings file.</source>
        <translation>Eksempler:&lt;br&gt;For terminal: &lt;i&gt;xterm -e %s&lt;/i&gt;&lt;br&gt;For å bytte bruker: &lt;i&gt;lxsudo %s&lt;/i&gt; eller &lt;i&gt;lxsudo dbus-run-session -- %s&lt;/i&gt;&lt;br&gt;&lt;i&gt;%s&lt;/i&gt; er kommandoen du ønsker å kjøre med terminal eller su.&lt;br&gt; Viktig: Vennligst bruk lxsudo, sudo alene ødelegger tillatelser på innstillingsfilen.</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="907"/>
        <source>Application restart is needed for changes to take effect.</source>
        <translation>Programmet må omstartes for at endringer skal tre i kraft.</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="139"/>
        <source>Bookmarks menu:</source>
        <translation>Bokmerkemeny:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="165"/>
        <source>Show folder context menu with Ctrl + right click</source>
        <translation>Vis mappens snareveismeny med Ctrl + høyreklikk</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="229"/>
        <source>Open folders in new tabs as far as possible</source>
        <translation>Åpne mapper i nye faner så langt det er mulig</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="232"/>
        <source>Single window mode</source>
        <translation>Enkeltvindusmodus</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="375"/>
        <source>Always show real file names</source>
        <translation>Vis alltid ordentlige filnavn</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="392"/>
        <source>Do not show file tooltips</source>
        <translation>Ikke vis fil-verktøytips</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="611"/>
        <source>Needs ffmpegthumbnailer</source>
        <translation>Trenger ffmpegthumbnailer</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="717"/>
        <source>Auto Mount</source>
        <translation>Automatisk innlasting</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="723"/>
        <source>Mount mountable volumes automatically on program startup</source>
        <translation>Last inn monterbare disker autmatisk ved programstart</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="730"/>
        <source>Mount removable media automatically when they are inserted</source>
        <translation>Lst inn uttagbare medier automatisk når de settes inn</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="737"/>
        <source>Show available options for removable media when they are inserted</source>
        <translation>Vis valg for uttagbare medier når de settes inn</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="747"/>
        <source>When removable medium unmounted:</source>
        <translation>Når et uttagbart medium avmonteres:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="753"/>
        <source>Close &amp;tab containing removable medium</source>
        <translation>Lukk &amp;faner der uttagbare medier er åpne</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="760"/>
        <source>Chan&amp;ge folder in the tab to home folder</source>
        <translation>Skift mappe i fanen til h&amp;jemmemappen</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="787"/>
        <source>Programs</source>
        <translation>Programmer</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="796"/>
        <source>Terminal emulator:</source>
        <translation>Terminalemulator:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="803"/>
        <source>Switch &amp;user command:</source>
        <translation>Skiftbr&amp;uker-kommando:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="829"/>
        <source>Archiver in&amp;tegration:</source>
        <translation>Komprimeringsin&amp;tegrasjon:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="852"/>
        <source>Templates</source>
        <translation>Maler</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="858"/>
        <source>Show only user defined templates in menu</source>
        <translation>Vis bare brukerdefinerte maler i meny</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="865"/>
        <source>Show only one template for each MIME type</source>
        <translation>Vis bare en mal for hver MIME-type</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="872"/>
        <source>Run default application after creation from template</source>
        <translation>Kjør standardprogram etter opprettelse fra mal</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="358"/>
        <source>Use SI decimal prefixes instead of IEC binary prefixes</source>
        <translation>Bruk SI desimalprefiks i stedet for IEC binærprefiks</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../bulkrename.cpp" line="73"/>
        <source>Renaming files...</source>
        <translation>Gir nytt navn til filer...</translation>
    </message>
    <message>
        <location filename="../bulkrename.cpp" line="73"/>
        <source>Abort</source>
        <translation>Avbryt</translation>
    </message>
    <message>
        <location filename="../bulkrename.cpp" line="82"/>
        <source>Warning</source>
        <translation>Advarsel</translation>
    </message>
    <message>
        <location filename="../bulkrename.cpp" line="82"/>
        <source>Renaming is aborted.</source>
        <translation>Bytte av navn er avbrutt.</translation>
    </message>
    <message>
        <location filename="../bulkrename.cpp" line="108"/>
        <location filename="../bulkrename.cpp" line="111"/>
        <location filename="../tabpage.cpp" line="1077"/>
        <source>Error</source>
        <translation>Feil</translation>
    </message>
    <message>
        <location filename="../bulkrename.cpp" line="108"/>
        <source>No file could be renamed.</source>
        <translation>Ingen fil kunne gis nytt navn.</translation>
    </message>
    <message>
        <location filename="../bulkrename.cpp" line="111"/>
        <source>Some files could not be renamed.</source>
        <translation>Noen filler kunne ikke gis nytt navn.</translation>
    </message>
    <message>
        <location filename="../tabpage.cpp" line="1077"/>
        <source>Cannot open as Admin.</source>
        <translation>Kan ikke åpnes som Admin.</translation>
    </message>
</context>
</TS>
